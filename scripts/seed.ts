import type { Prisma } from '@prisma/client'
import { db } from 'api/src/lib/db'

export default async () => {
  try {
    //
    // Manually seed via `yarn rw prisma db seed`
    // Seeds automatically with `yarn rw prisma migrate dev` and `yarn rw prisma migrate reset`
    //
    // Update "const data = []" to match your data model and seeding needs
    //
    const data: Prisma.PostCreateArgs['data'][] = [
      {
        title: 'First Post',
        body: 'Neutra tacos hot chicken prism raw denim, put a bird on it enamel pin post-ironic vape cred DIY. Street art next level umami squid. Hammock hexagon glossier 8-bit banjo. Neutra la croix mixtape echo park four loko semiotics kitsch forage chambray. Semiotics salvia selfies jianbing hella shaman. Letterpress helvetica vaporware cronut, shaman butcher YOLO poke fixie hoodie gentrify woke heirloom.',
        publishStatus: true,
      },
      {
        title: 'Second Post',
        body: "I'm baby chambray you probably haven't heard of them austin, portland normcore offal authentic waistcoat four loko. Ascot hella green juice, seitan synth heirloom you probably haven't heard of them swag hoodie selvage tilde asymmetrical tacos activated charcoal quinoa. Chillwave sartorial ugh tbh, street art glossier food truck pabst yes plz williamsburg bicycle rights sus taiyaki crucifix occupy. Kale chips actually JOMO, pinterest cardigan fashion axe biodiesel tbh listicle yr pok pok.",
        publishStatus: true,
      },
      {
        title: 'Third Post',
        body: "Meditation you probably haven't heard of them dreamcatcher yr, palo santo sartorial tattooed venmo fanny pack authentic selfies celiac scenester cold-pressed. Cred vice shaman fam hoodie tbh asymmetrical gentrify pop-up prism cold-pressed cray echo park ennui tofu. Activated charcoal echo park mustache, yes plz master cleanse jean shorts etsy kitsch forage stumptown umami ramps big mood hot chicken aesthetic. Man braid hammock forage, banjo adaptogen vaporware slow-carb flannel tumblr try-hard.",
        publishStatus: true,
      },
      {
        title: 'Fourth Post',
        body: "I'm baby cardigan church-key offal cliche cornhole selfies fam pok pok art party salvia post-ironic tattooed before they sold out. Mlkshk paleo authentic helvetica readymade ethical. Sartorial fanny pack edison bulb, fit sriracha man braid cray letterpress kinfolk four loko vibecession paleo viral pickled. Squid brunch biodiesel drinking vinegar, selfies XOXO vice narwhal letterpress church-key pinterest. Raclette tilde vexillologist ethical vibecession cold-pressed. +1 asymmetrical mukbang, lo-fi readymade copper mug hell of jianbing messenger bag shabby chic squid.",
        publishStatus: true
      },
    ]
    console.log(
      "\nUsing the default './scripts/seed.{js,ts}' template\nEdit the file to add seed data\n"
    )

    // Note: if using PostgreSQL, using `createMany` to insert multiple records is much faster
    // @see: https://www.prisma.io/docs/reference/api-reference/prisma-client-reference#createmany
    Promise.all(
      //
      // Change to match your data model and seeding needs
      //
      data.map(async (data: Prisma.PostCreateArgs['data']) => {
        const record = await db.post.create({ data })
        console.log(record)
      })
    )
  } catch (error) {
    console.warn('Please define your seed data.')
    console.error(error)
  }
}
