import { Prisma } from '.prisma/client';
import type { QueryResolvers, CommentResolvers } from 'types/graphql'

import { db } from 'src/lib/db'

interface CreateCommentsArgs {
  input: Prisma.CommentCreateArgs
}

export const comments: QueryResolvers['comments'] = () => {
  return db.comment.findMany()
}

// export const comment: QueryResolvers['comment'] = ({ id }) => {
//   return db.comment.findUnique({
//     where: { id },
//   })
// }

export const Comment: CommentResolvers = {
  post: (_obj, { root }) =>
    db.comment.findUnique({ where: { id: root.id } }).post(),
}

export const createComment: QueryResolvers['createComment'] = async ({ input }) => {
  return db.comment.create({ data: input })
}

export const deleteComment: QueryResolvers['deleteComment'] = async ({ id }) => {
  return db.comment.delete({ where: { id } })
}