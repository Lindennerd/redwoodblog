import { MetaTags } from '@redwoodjs/web'

const AboutPage = () => {
  return (
    <>
      <MetaTags title="About" description="About page" />
      <div>
        About Page
      </div>
    </>
  )
}

export default AboutPage
