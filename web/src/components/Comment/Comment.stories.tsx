import Comment from './Comment'

export const generated = (args) => {
  return (
    <Comment
      comment={{
        name: 'John Doe',
        body: 'het sic dragons',
        createdAt: '2022-06-08T00:00:00.000Z',
      }}
    />
  )
}

export default { title: 'Components/Comment' }
