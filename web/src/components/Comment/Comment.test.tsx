import { render,screen } from '@redwoodjs/testing/web'

import Comment from './Comment'

//   Improve this test with help from the Redwood Testing Doc:
//    https://redwoodjs.com/docs/testing#testing-components

describe('Comment', () => {
  it('renders successfully', () => {
    const comment = {
      name: 'John Doe',
      body: 'het sic dragons',
      createdAt: '2022-06-08T00:00:00.000Z',
    }

    expect(() => {
      render(<Comment comment={comment} />)
    }).not.toThrow()

    render(<Comment comment={comment} />)

    expect(screen.getByText(comment.name)).toBeInTheDocument()
    expect(screen.getByText(comment.body)).toBeInTheDocument()
    expect(screen.getByText(comment.createdAt)).toBeInTheDocument()
  })
})
