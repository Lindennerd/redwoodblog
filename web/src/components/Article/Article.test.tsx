import { render } from '@redwoodjs/testing/web'
import { standard } from '../ArticleCell/ArticleCell.mock'
import { screen } from '@testing-library/react'

import Article from './Article'

//   Improve this test with help from the Redwood Testing Doc:
//    https://redwoodjs.com/docs/testing#testing-components

describe('Article', () => {
  it('renders a blog post', () => {
    const article = standard().article;
    render(<Article article={article} />)

    expect(screen.getByText(article.title)).toBeInTheDocument()
    expect(screen.getByText(article.body)).toBeInTheDocument()
  })
})
